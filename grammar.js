const PREC = {
  RETURN:  [1, 'right', 'return'],
  ASSIGN:  [2, 'right', choice('=', '%=', '^=', '&=', '*=', '**=', '-=', '+=',
              '|=', '<<=', '>>=', '/=')],
  COMMA:   [3, 'left', ','],
  DOTDOT:  [4, 'left', '..'],
  ORELSE:  [5, 'left', '||'],
  ANDALSO: [6, 'left', '&&'],
  EQUAL:   [7, 'left', choice('==', '!=')],
  OR:      [8, 'left', '|'],
  XOR:     [9, 'left', '^'],
  AND:     [10, 'left', '&'],
  CMP:     [11, 'left', choice('<', '<=', '>', '>=')],
  SHIFT:   [12, 'left', choice('<<', '>>')],
  ADDSUB:  [13, 'left', choice('+', '-')],
  MULDIV:  [14, 'left', choice('%', '*', '/')],
  POW:     [15, 'right', '**'],
  PREFIX:  [16, 'right', choice('~', '!', '-', '+', '*', '**', '&', '--', '++')],
  POSTFIX: [17, 'left', choice('!', '?')],
  TYPE_POSTFIX: [18, 'left', choice('!', '?')],
  TYPE_PREFIX: [19, 'right', choice('*', '**', '&')],
  DOT:     [20, 'left', '.'],
  SCOPE:   [21, 'left', '::'],
  EXPR:    [1],
};

module.exports = grammar({
  name: 'lava',
  word: $ => $.ident,
  extras: $ => [/\s/, $._line_comment, $._block_comment],

  rules: {
    source_file: $ => repeat($._decl),

    _line_comment: $ => token(seq('//', /.*/)),
    _block_comment: $ => token(seq('/*', /[^*]*\*+([^/*][^*]*\*+)*/, '/')),

    // Literals {{{

    ident: $ => /[A-Za-z_][A-Za-z0-9_]*/,

    true: $ => 'true',
    false: $ => 'false',
    null: $ => 'null',
    void: $ => 'void',

    string: $ => choice(
      /'[^']+'/,
      /"[^"]+"/
    ),

    int_literal: $ => /[0-9]+/,
    hex_literal: $ => /0[Xx][0-9A-Fa-f]+/,
    bin_literal: $ => /0[Bb][01]+/,
    float_literal: $ => /([0-9]+\.[0-9]*)|(\.[0-9]+)/,

    _literal: $ => choice(
      $.int_literal,
      $.hex_literal,
      $.bin_literal,
      $.float_literal,
      $.string,
      $.true,
      $.false,
      $.null,
      $.void,
    ),

    // }}}

    // Namespaces {{{

    _decl: $ => choice(
      $.namespace_decl,
      $._stmt,
    ),

    relative_ident: $ => seq(
      $.ident,
      repeat(seq('::', $.ident))
    ),

    namespace_decl: $ => seq(
      'namespace',
      field('name', $.relative_ident),
      choice(';', field('body', $.namespace_body))
    ),

    namespace_body: $ => seq(
      '{',
      repeat($._decl),
      '}'
    ),

    // }}}

    // Functions {{{

    fun_decl: $ => seq(
      'fun',
      field('name', $.ident),
      '(',
      optional(field('args', $.arg_list)),
      ')',
      optional(field('return', $.return_spec)),
      choice(';', field('body', $.fun_body))
    ),

    fun_expr: $ => seq(
      'fun',
      '(',
      optional(field('args', $.arg_list)),
      ')',
      optional(field('return', $.return_spec)),
      field('body', $.fun_body)
    ),

    arg_list: $ => seq(
      $.arg,
      repeat(seq(',', $.arg)),
      optional(',')
    ),

    arg: $ => seq(
      field('type', $._type_expr),
      optional(field('name', $.ident))
    ),

    return_spec: $ => seq('->', field('type', $._type_expr)),

    fun_body: $ => seq(
      '{',
      repeat($._stmt),
      '}'
    ),

    // }}}

    // Variables {{{

    var_decl: $ => seq(
      choice('let', 'mutable'),
      optional(field('type', $._type_expr)),
      field('name', $.ident),
      optional($._var_init),
      repeat(seq(
        ',',
        field('name', $.ident),
        optional($._var_init)
      ))
    ),

    _var_init: $ => seq(
      '=',
      field('expr', $._expr)
    ),

    // }}}

    // Types {{{

    _type_stmt: $ => choice(
      $.empty_stmt,
      seq($.var_decl, ';'),
      $.type_decl,
      $.fun_decl
    ),

    _type_expr: $ => choice(
      $.ident,
      $.null,
      $.void,
      $.type_prefix_expr,
      $.type_postfix_expr,
      $.type_binary_expr,
      $.type_call_expr,
      $.bracket_expr
    ),

    type_prefix_expr: $ => choice(
      ...[
        PREC.TYPE_PREFIX,
        PREC.DOT,
        PREC.SCOPE,
      ].map(p =>
        prec.right(p[0], seq(
          field('operator', p[2]),
          field('expr', $._type_expr)
        ))
      )
    ),

    type_postfix_expr: $ => prec.left(PREC.TYPE_POSTFIX[0], seq(
      field('expr', $._type_expr),
      field('operator', PREC.TYPE_POSTFIX[2])
    )),

    type_call_expr: $ => choice(
      ...[
        ['(', ')'],
        ['[', ']']
      ].map(([open, close]) =>
        prec.left(PREC.TYPE_POSTFIX[0], seq(
          field('left', $._type_expr),
          field('open', open),
          optional(field('right', $._expr)),
          field('close', close)
        ))
      )
    ),

    type_binary_expr: $ => choice(
      ...[
        PREC.OR,
        PREC.AND,
        PREC.DOT,
        PREC.SCOPE,
      ].map(p =>
        prec[p[1]](p[0], seq(
          field('left', $._type_expr),
          field('operator', p[2]),
          field('right', $._type_expr)
        ))
      )
    ),

    type_decl: $ => seq(
      choice('struct', 'union', 'interface'),
      field('name', $.ident),
      optional(seq('(', field('args', $.arg_list), ')')),
      optional(seq(':', $.type_extends_list)),
      '{',
      repeat($._type_stmt),
      '}'
    ),

    type_extends_list: $ => seq(
      $._type_expr,
      repeat(seq(',', $._type_expr))
    ),

    // }}}

    // Statements and expressions {{{

    empty_stmt: $ => ';',

    _stmt: $ => choice(
      $._type_stmt,
      seq($._expr, ';'),
      $._control_flow
    ),

    _expr: $ => choice(
      prec(PREC.EXPR[0], $.ident),
      $._literal,
      $.prefix_expr,
      $.postfix_expr,
      $.binary_expr,
      $.call_expr,
      $.bracket_expr,
      $.fun_expr,
      prec(PREC.EXPR[0], $._control_flow)
    ),

    prefix_expr: $ => choice(
      ...[
        PREC.RETURN,
        PREC.DOTDOT,
        PREC.PREFIX,
        PREC.DOT,
        PREC.SCOPE,
      ].map(p =>
        prec.right(p[0], seq(
          field('operator', p[2]),
          field('expr', $._expr)
        ))
      )
    ),

    postfix_expr: $ => choice(
      ...[
        PREC.DOTDOT,
        PREC.POSTFIX,
      ].map(p =>
        prec.left(p[0], seq(
          field('expr', $._expr),
          field('operator', p[2])
        ))
      )
    ),

    call_expr: $ => choice(
      ...[
        ['(', ')'],
        ['[', ']']
      ].map(([open, close]) =>
        prec.left(PREC.POSTFIX[0], seq(
          field('left', $._expr),
          field('open', open),
          optional(field('right', $._expr)),
          field('close', close)
        ))
      )
    ),

    binary_expr: $ => choice(
      ...[
        PREC.ASSIGN,
        PREC.COMMA,
        PREC.DOTDOT,
        PREC.ORELSE,
        PREC.ANDALSO,
        PREC.EQUAL,
        PREC.OR,
        PREC.XOR,
        PREC.AND,
        PREC.CMP,
        PREC.SHIFT,
        PREC.ADDSUB,
        PREC.MULDIV,
        PREC.POW,
        PREC.DOT,
        PREC.SCOPE
      ].map(p =>
        prec[p[1]](p[0], seq(
          field('left', $._expr),
          field('operator', p[2]),
          field('right', $._expr)
        ))
      )
    ),

    bracket_expr: $ => choice(
      seq('(', optional(field('expr', $._expr)), ')'),
      seq('[', optional(field('expr', $._expr)), ']')
    ),

    // }}}

    // Control flow {{{

    _control_flow: $ => choice(
      $.label,
      $.brace_stmt,
      $.if_stmt,
      $.while_stmt,
    ),

    label: $ => seq($.ident, ':'),

    brace_stmt: $ => seq(
      '{',
      repeat($._stmt),
      '}'
    ),

    if_stmt: $ => seq(
      'if',
      field('expr', $._expr),
      '{',
      field('body', repeat($._stmt)),
      '}',
      optional(seq('else', '{', field('else', repeat($._stmt)), '}'))
    ),

    while_stmt: $ => seq(
      'while',
      field('expr', $._expr),
      '{',
      field('body', repeat($._stmt)),
      '}'
    ),

    // }}}
  }
});
